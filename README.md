# Research Interests

This page hopefully features the past, current and future research interests of our institute members.

If you are looking for a guided tour of the project, watch [https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4](https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4) (DE)

If you want to contribute a research topic, see the [CONTRIBUTING.md](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/blob/master/CONTRIBUTING.md).

## List of Sites

### `Master`

The [master](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/tree/master) is currently delivered to 
* [https://www3.tuhh.de/cfg0846/research-topics-mat-tuhh/](https://www3.tuhh.de/cfg0846/research-topics-mat-tuhh/) 
* [https://fabiangabel.gitlab.io/research-topics-mat-tuhh](https://fabiangabel.gitlab.io/research-topics-mat-tuhh)

### `Dev`

The development version of this webpage for review is currently delivered to [https://review.fabian-gabel.de/dev](https://review.fabian-gabel.de/dev).

### Review Apps

[GitLab Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) can be accessed on 
* [https://www3-dev.tuhh.de/cfg0846/research-topics-mat-tuhh/](https://www3-dev.tuhh.de/cfg0846/research-topics-mat-tuhh/) 
* [https://review.fabian-gabel.de/](https://review.fabian-gabel.de/)

### GitLab Repository

The corresponding [GitLab](https://collaborating.tuhh.de) repository is 
* hosted at [https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh) 
* [push-mirrored](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#pushing-to-a-remote-repository) to [https://gitlab.com/fabiangabel/research-topics-mat-tuhh](https://gitlab.com/fabiangabel/research-topics-mat-tuhh)
