# Laplace transforms for generalised functions and the abstract Cauchy problem

### Working Groups: aa

### Collaborators (MAT): kkruse

## Description

Let $E$ be a (sequentially) complete complex locally convex Hausdorff space ($\mathbb{C}$-lcHs). The initial value problem

$$
\begin{align}
x'(t)&=Ax(t),\quad t>0,\\
x(0)&=x_{0}\in E,
\end{align}
$$

is called an *abstract Cauchy problem* where 

$$
A\colon D(A)\subset E\to E
$$

is a (sequentially) closed linear operator with domain $D(A)$. We study the abstract Cauchy problem in the space of $E$-valued [hyperfunctions](https://en.wikipedia.org/wiki/Hyperfunction) with support in $[0,\infty)$. Hyperfunctions were introduced by Sato [10,11] and extended to Fourier hyperfunctions by Kawai [2]. Hyperfunctions form a quite large class of generalised functions, containing locally integrable functions, distributions and ultradistributions. 

We study Fourier and Laplace transforms for Fourier hyperfunctions with values in a $\mathbb{C}$-lcHs. Since any hyperfunction with values in a wide class of locally convex Hausdorff spaces can be extended to a Fourier hyperfunction [4,5], this gives simple notions of asymptotic Fourier and Laplace transforms for vector-valued hyperfunctions [6], which improves the existing models of Komatsu [3], Bäumer [1], Lumer and Neubrander [9] and Langenbruch [8]. We apply our theory of asymptotic Laplace transforms to prove existence and uniqueness results for solutions of the abstract Cauchy problem in a wide class of locally convex Hausdorff spaces, containing Fréchet spaces and several spaces of distributions [7].

## References

[1] B. Bäumer. A vector-valued operational calculus and abstract Cauchy problems. PhD thesis, Louisiana State University, Baton Rouge, LA, 1997. Available at [LSU Digital Commons](https://digitalcommons.lsu.edu/gradschool_disstheses/6464/).

[2] T. Kawai. On the theory of Fourier hyperfunctions and its applications to partial differential equations with constant coefficients. *J. Fac. Sci. Univ. Tokyo, Sect. IA*, 17:467--517, 1970. doi: [10.15083/00039821](https://doi.org/10.15083/00039821).

[3] H. Komatsu. Laplace transforms of hyperfunctions -- A new foundation of the Heaviside calculus. *J. Fac. Sci. Univ. Tokyo, Sect. IA*, 34:805--820, 1987. doi: [10.15083/00039471](https://doi.org/10.15083/00039471).

[4] K. Kruse. Vector-valued Fourier hyperfunctions. PhD thesis, Universität Oldenburg, 2014. URN: [urn:nbn:de:gbv:715-oops-19095](http://nbn-resolving.org/urn:nbn:de:gbv:715-oops-19095).

[5] K. Kruse. Vector-valued Fourier hyperfunctions and boundary values, 2019. [arXiv:1912.03659](https://arxiv.org/abs/1912.03659).

[6] K. Kruse. Asymptotic Fourier and Laplace transforms for vector-valued hyperfunctions, 2021. [arXiv:2104.02682](https://arxiv.org/abs/2104.02682).

[7] K. Kruse. The abstract Cauchy problem in locally convex spaces. *In preparation*.

[8] M. Langenbruch. Asymptotic Fourier and Laplace transformations for hyperfunctions. *Stud. Math.*, 205(1):41--69, 2011. doi: [10.4064/sm205-1-4](https://doi.org/10.4064/sm205-1-4).

[9] G. Lumer, F. Neubrander. The asymptotic Laplace transform: New results and relation to Komatsu’s Laplace transform of hyperfunctions. In F. Mehmeti, J. von Below, S. Nicaise, editors, *Partial differential equations on multistructures*, volume 219 of *Notes Pure Appl. Math.*, 147--162, Dekker, New York, 2001. doi: [10.1201/9780203902196](https://doi.org/10.1201/9780203902196).

[10] M. Sato. Theory of hyperfunctions, I. *J. Fac. Sci. Univ. Tokyo, Sect. IA*, 8:139--193, 1959. doi: [10.15083/00039918](https://doi.org/10.15083/00039918).

[11] M. Sato. Theory of hyperfunctions, II. *J. Fac. Sci. Univ. Tokyo, Sect. IA*, 8:387--437, 1960. doi: [10.15083/00039916](https://doi.org/10.15083/00039916).
