# E-learning and e-assessment in mathematics

### Working Groups: aa

### Collaborators (MAT): cseifert, dgallaun, kkruse

### External Collaborators: [Leo Dostal](https://www.tuhh.de/mum/mitarbeiter/wissenschaftliche-mitarbeiter/dr-leo-dostal.html), [Mirjam S. Glessmer](https://mirjamglessmer.com/), [Natalia Konchakova](https://www.hereon.de/institutes/surface_science/interface_modeling/team/098961/index.php.de)

## Description

At many universities, undergraduate courses of fundamental subjects such as mathematics are taught to students enrolled in many different course programs. Since the fundamental subject is then taught without using examples from the students’ main subjects, this often results in low student motivation in the fundamental subject and lacking knowledge and skills of this subject even when necessary for the understanding of the students’ main subject. At Hamburg University of Technology, this applies to first year mathematics, which is taught in the same course to 1300 students enrolled in 13 different engineering study programs.

Our approach to solve this dilemma: 

* Provide oportunities for individual practice,
* feedback tailored to the needs of students of the different study courses such that students apply mathematical concepts in the context of their main subject. 

This helps them to see the relevance of the content which might otherwise be perceived as bothersome and irrelevant, thereby increasing student motivation. Providing additional practice opportunities also increases perceived student self-efficacy, in turn enhancing motivation [3]. We use electronic exercises and an e-assessment system to cope with the sheer number of students, and problems from mechanical engineering [3,4,5,6] as well as electrical engineering [2] to demonstrate the linking of mathematics and engineering science.

The e-assessment system also helps to manage another problem caused by the very large number of participants of the exam in first year mathematics, namely the correspondingly high examination effort. Conducting such an exam electronically might on the one hand significantly reduce the amount of work (automated correction and grading) and on the other hand provide a new dimension of possible exam questions. But in order to develop good questions for such an electronic exam, one has to address different aspects [1], e.g.:

* Randomisation of parameters such that all realisations are comparable for fairness,
* provide open-ended questions to demonstrate understanding rather than guesses,
* handling of errors, i.e. how to check for mistakes made by students.

Of course an electronic exam is not restricted to large courses in first year mathematics. As in many courses on applied mathematics, in order to give a glimpse on realistic problems, one is faced with large computations which are typically done by computers. However, when it comes to exams on such topics students are often asked to apply the learned methods, which are suited for large systems, to very small problems by pen-and-paper. This gap can be overcome by means of an electronic exam [7].

## References

[1] D. Gallaun, K. Kruse, C. Seifert. Adaptive Übungs- und Prüfungsaufgaben in Mathematik mit hochwertiger Bewertung. In D. Schott, editor, *Proceedings 15. Workshop Mathematik in ingenieurwissenschaftlichen Studiengängen*, Heft 02/2019, 18--24, Gottlob-Frege-Zentrum, Wismar, 2019. Avaliable at [HS Wismar](https://kompetenz.hs-wismar.de/index.php/Proceedings_15._Workshop_Mathematik_in_ingenieurwissenschaftlichen_Studieng%C3%A4ngen_Rostock-Warnem%C3%BCnde_2019).

[2] D. Gallaun, K. Kruse, C. Seifert. Anwendungsbezogene elektronische Übungsaufgaben in Ingenieurmathematik, *55. Jahrestagung der Gesellschaft für Didaktik der Mathematik (GDM)*, 2021. *Preprint*.

[3] M.S. Glessmer, C. Seifert. E-Assessments to increase the perceived importance of Mathematics in the introductory phase of Engineering Education via bridging tasks. In J.C. Quadrado, J. Bernardino, J. Rocha, editors, *Proceedings of Sefi Annual Conferences 2017*, 1549--1556, 2017. Available at [SEFI](https://www.sefi.be/wp-content/uploads/).

[4] M.S. Glessmer, C. Seifert, L. Dostal, N. Konchakova, K. Kruse. Individualisierung von Großveranstaltungen. Oder: Wie man Ingenieurstudierenden die Mathematik schmackhaft macht. In W.D. Paravicini, J. Schnieder, editors, *Hanse-Kolloquium zur Hochschuldidaktik der Mathematik 2015*, 64--75, wtm-Verlag, Münster, 2016. Available at [WWU Münster](https://miami.uni-muenster.de/Record/69a62744-78ea-42b5-856d-4cd43c042602).

[5] M.S. Glessmer, C. Seifert, L. Dostal, N. Konchakova, K. Kruse. Providing opportunities for individual practice and assessment in a large undergraduate mathematics course. In P. Kapranos, editor, *International Symposium on Engineering Education -- Interdisciplinary
Engineering -- Breaking Boundaries, ISEE 2016 Conference Proceedings*, 13--20, TJ International, Padstow, UK, 2016. doi: [10.15131/shef.data.3507380.v1](https://doi.org/10.15131/shef.data.3507380.v1).

[6] K. Kruse, L. Dostal, M.S. Glessmer, N. Konchakova, C. Seifert. Conception of online e-assessment exercises for math courses with elements from mechanical engineering. In G. Kammasch, H. Klaffke, S. Knutzen, editors, *Tagungsband der 11. Ingenieurpädagogischen Regionaltagung*, Hamburg, 232--236, 2017. doi: [10.15480/882.1394](https://doi.org/10.15480/882.1394).

[7] K. Kruse, C. Seifert. Implementing Computer-assisted Exams in a Course on Numerical Analysis for Engineering Students. In ISEC, Portugal, editors, *Proceedings of the 19th SEFI MWG*, Coimbra, Portugal, 33--38, 2018. Available at [SEFI MWG](http://sefi.htw-aalen.de/).
