# Semi-open queueing networks and robotic mobile fulfillment systems

### Working Groups: aa

### Collaborators (MAT): kkruse, sotten

### Collaborators (External): [Prof. Dr. Hans Daduna](https://www.math.uni-hamburg.de/home/daduna/), [Dr. Ruslan Krenzler](https://www.researchgate.net/profile/Ruslan-Krenzler), [Prof. Dr. Lin Xie](https://www.leuphana.de/institute/iis/personen/lin-xie.html)

## Description

We consider a semi-open queueing network (SOQN), where a customer requires one resource from the resource pool for service. If there is a resource available,  the resource enters an inner network to complete the customer's order. If there is no resource available, the new customer has to wait in an external queue until one becomes available "backordering". When a resource exits the inner network, it is returned to the resource pool.

We present a new solution approach. To approximate the inner network with the resource pool, we consider a  odification, where newly arriving customers are lost if the resource pool is empty. We adjust the arrival rate of the modified system so that the throughputs in each node are pairwise identical to those in the original network. To approximate the external queue of the SOQN with  ackordering, we construct a reduced SOQN with backordering by using results from the lost-customers modification.

We apply our results to robotic mobile fulfilment systems (RMFSs). Instead of sending pickers to the storage area to search for the ordered items and pick them, robots carry shelves with ordered items from the storage area to picking stations. We model the RMFS as an SOQN to determine the minimal number of robots.

<style>
img {
    max-width: 100%;
    height: auto;      
}
</style>
<div align="center">
<img src="img/Lost-customers-approximation-of-SOQN.jpg" alt='Overview of the models'>
</div>

## References

[1] Lost-customers approximation of semi-open queueing networks with backordering -- An application to minimise the number of robots in robotic mobile fulfilment systems. S. Otten, R. Krenzler, L. Xie, H. Daduna, K. Kruse. Available at [arXiv:1912.01782](https://arxiv.org/abs/1912.01782
)
