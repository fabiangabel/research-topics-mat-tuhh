# Linearisation of vector-valued functions

### Working Groups: aa

### Collaborators (MAT): kkruse

## Description

It is a classical idea to represent vector-valued functions by continuous linear operators [13]. Let $\mathcal{F}(\Omega)$ be a locally convex Hausdorff space (lcHs) of functions from a set $\Omega$ to the field $\mathbb{K}$ of real or complex numbers and $E$ an lcHs over $\mathbb{K}$. Then Schwartz' $\varepsilon$-product of $\mathcal{F}(\Omega)$ and $E$ is defined as the space of continuous linear operators 

$$
\mathcal{F}(\Omega)\varepsilon E :=L_{e}(\mathcal{F}(\Omega)_{\kappa}',E).
$$

Supposing that the point-evaluations $\delta_{x}$ belong to the dual space $\mathcal{F}(\Omega)'$ for all $x\in\Omega$ and that there is an lcHs $\mathcal{F}(\Omega,E)$ consisting of $E$-valued functions on $\Omega$ which is the counterpart of $\mathcal{F}(\Omega)$, linearisation of $\mathcal{F}(\Omega,E)$ means that the map 

$$
S\colon \mathcal{F}(\Omega)\varepsilon E \to \mathcal{F}(\Omega,E),\; u\longmapsto[x\mapsto u(\delta_{x})],
$$

is a well-defined topological isomorphism. 

In [10] we derive sufficient conditions on $E$ and on the properties and structures of the functions and function spaces $\mathcal{F}(\Omega)$ and $\mathcal{F}(\Omega,E)$ such that the map $S$ is a topological isomorphism. Once the isomorphism $S$ is established, the famous [approximation property](https://en.wikipedia.org/wiki/Approximation_property) of a space $\mathcal{F}(\Omega)$ is equivalent to the property that every function in $\mathcal{F}(\Omega,E)$ can be approximated by functions with values in finite dimensional subspaces of $E$ for any lcHs $E$, which we investigate in [4] for weighted spaces of $\mathcal{C}^{k}$-smooth functions. In [7] we study the stronger property that $\mathcal{F}(\Omega)$ is [nuclear](https://en.wikipedia.org/wiki/Nuclear_space) in the case of weighted $\mathcal{C}^{\infty}$-smooth functions. 

Nuclearity can be used to transfer the surjectivity of a continuous linear map $T\colon \mathcal{F}(\Omega)\to\mathcal{F}(\Omega)$ to the $\varepsilon$-product $T\varepsilon \operatorname{id}_{E}\colon \mathcal{F}(\Omega)\varepsilon E\to\mathcal{F}(\Omega)\varepsilon E$ for Fréchet spaces $\mathcal{F}(\Omega)$ and $E$ by Grothendieck's classical [tensor product](https://en.wikipedia.org/wiki/Topological_tensor_product) theory [1]. In combination with the topological isomorphism $S$ this implies that the surjectivity of a continuous linear partial differential operator can be transfered from the scalar-valued to the vector-valued case, which we study for the Cauchy-Riemann operator $T=\overline{\partial}$ on weighted spaces of $\mathcal{C}^{\infty}$-smooth functions in [2,5,6,8] even for $E$ beyond the class of Fréchet spaces.

Another application of the topological isomorphism $S$ lies in lifting series representations from scalar-valued to $E$-valued functions [12], for instance power series representations of holomorphic functions [9], and the extension of $E$-valued functions via weak extensions [3,11], i.e. to answer the question:

Let $\Lambda$ be a subset of $\Omega$ and $G$ a linear subspace of $E'$. Let $f\colon \Lambda\to E$ be such that for every $e'\in G$, the function $e'\circ f\colon\Lambda\to \mathbb{K}$ has an extension in $\mathcal{F}(\Omega)$. When is there an extension $F\in\mathcal{F}(\Omega,E)$ of $f$, i.e. $F_{\mid \Lambda} = f$ ?

## References

[1] A. Grothendieck. Produits tensoriels topologiques et espaces nucléaires. *Mem. Amer. Math.
Soc. 16*. AMS, Providence, RI, 1955. doi: [10.1090/memo/0016](https://doi.org/10.1090/memo/0016).

[2] K. Kruse. Surjectivity of the $\overline{\partial}$-operator between spaces of weighted smooth vector-valued functions, 2018. [arXiv:1810.05069](https://arxiv.org/abs/1810.05069).

[3] K. Kruse. Extension of vector-valued functions and sequence space representation, 2019. [arXiv:1808.05182](https://arxiv.org/abs/1808.05182).

[4] K. Kruse. The approximation property for weighted spaces of differentiable functions. In M. Kosek, editor, *Function Spaces XII*, volume 119 of *Banach Center Publ.*, 233--258, Inst. Math., Polish Acad. Sci., Warszawa, 2019. doi: [10.4064/bc119-14](https://doi.org/10.4064/bc119-14).

[5] K. Kruse. The Cauchy-Riemann operator on smooth Fréchet-valued functions with exponential growth on rotated strips. *PAMM*, 19(1):1--2, 2019. doi: [10.1002/pamm.201900141](https://doi.org/10.1002/pamm.201900141).

[6] K. Kruse. The inhomogeneous Cauchy-Riemann equation for weighted smooth vector-valued functions on strips with holes, 2019. [arXiv:1901.02093](https://arxiv.org/abs/1901.02093).

[7] K. Kruse. On the nuclearity of weighted spaces of smooth functions. *Ann. Polon. Math.*, 124(2):173--196, 2020. doi: [10.4064/ap190728-17-11](https://doi.org/10.4064/ap190728-17-11).

[8] K. Kruse. Parameter dependence of solutions of the Cauchy-Riemann equation on weighted spaces of smooth functions. *RACSAM Rev. R. Acad. Cienc. Exactas Fís. Nat. Ser. A Mat.*, 114(141):1--24, 2020. doi: [10.1007/s13398-020-00863-x](https://doi.org/10.1007/s13398-020-00863-x).

[9] K. Kruse. Vector-valued holomorphic functions in several variables. *Funct. Approx. Comment. Math.*, 63(2):247--275, 2020. doi: [10.7169/facm/1861](https://doi.org/10.7169/facm/1861).

[10] K. Kruse. Weighted spaces of vector-valued functions and the $\varepsilon$-product, *Banach J. Math. Anal.*, 14(4):1509--1531, 2020. doi: [10.1007/s43037-020-00072-z](https://doi.org/10.1007/s43037-020-00072-z).

[11] K. Kruse. Extension of vector-valued functions and weak-strong principles for differentiable functions of finite order, 2021. [arXiv:1910.01952](https://arxiv.org/abs/1910.01952).

[12] K. Kruse. Series representations in spaces of vector-valued functions via Schauder decompositions. *Math. Nachr.*, 294(2):354--376, 2021. doi: [10.1002/mana.201900172](https://doi.org/10.1002/mana.201900172).

[13] L. Schwartz. Espaces de fonctions différentiables à valeurs vectorielles. *J. Analyse Math.*, 4:88--148, 1955. doi: [10.1007/BF02787718](https://doi.org/10.1007/BF02787718).
