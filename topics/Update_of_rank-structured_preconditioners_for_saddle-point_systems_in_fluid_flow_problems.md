# Update of rank-structured preconditioners for saddle-point systems in fluid flow problems

### Working Groups: nm

### Collaborators (MAT): rbeddig, sleborne 

### Collaborators (External): [Prof. Dr. Jörn Behrens](https://www.math.uni-hamburg.de/home/behrens/), [Dr. Konrad Simon]

## Description

The simulation of fluid-flow problems using the Stokes or Navier-Stokes equation requires the numerical solution of saddle point problems. The main difficulty here lies in solution methods for the dense Schur complement. Furthermore, we often have to solve repeatedly similar linear systems as in optimal design, in linearization methods, or in adaptive discretizations. This requires suitable preconditioning techniques and efficient update methods. In my research, I am developing preconditioners for the Schur complement using rank-structured matrices. Furthermore, I plan to work on efficient update techniques for these preconditioners. The derived methods will be applied to atmospheric models described by the Boussinesq approximationThe simulation of fluid-flow problems using the Stokes or Navier-Stokes equation requires the numerical solution of saddle point problems. The main difficulty here lies in solution methods for the dense Schur complement. Furthermore, we often have to solve repeatedly similar linear systems as in optimal design, in linearization methods, or in adaptive discretizations. This requires suitable preconditioning techniques and efficient update methods. In my research, I am developing preconditioners for the Schur complement using rank-structured matrices. Furthermore, I plan to work on efficient update techniques for these preconditioners. The derived methods will be applied to atmospheric models described by the Boussinesq approximation.