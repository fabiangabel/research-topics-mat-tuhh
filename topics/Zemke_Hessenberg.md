# Algebraic Properties of Eigenvalues and Eigenvectors of Hessenberg Matrices

### Working Groups: nm

### Collaborators (MAT): jpmzemke

## Description

Eigenvectors and principal vectors of unreduced Hessenberg matrices can be expressed by polynomial vectors and their derivatives evaluated at the eigenvalues. This point of view extends to the elements of the adjugate and the inverse, as well as the components of the LU decomposition. The relations obtained in [4,5] allow for a refined analysis of the convergence of the QR algorithm, Rayleigh quotient iteration (RQI), and other Krylov subspace methods.

Current research focuses on the reachable accuracy in the computation of eigenvalues and eigenvectors in finite precision. This is primarily of interest in a class of methods related to induced dimension reduction (IDR) by Sonneveld and van Gijzen [3], see [1,2,6]. We aim at both classifying the existing methods and developing fast and reliable new methods.

Interesting extensions would be the treatment of reduced Hessenberg matrices and unreduced block Hessenberg matrices.

## References

[1] Gutknecht, Martin H.; Zemke, Jens-Peter M. Eigenvalue computations based on IDR. SIAM J. Matrix Anal. Appl. 34 (2013), no. 2, 283--311.

[2] Rendel, Olaf; Rizvanolli, Anisa; Zemke, Jens-Peter M. IDR: a new generation of Krylov subspace methods? Linear Algebra Appl. 439 (2013), no. 4, 1040--1061.

[3] Sonneveld, Peter; van Gijzen, Martin B. IDR(s): a family of simple and fast algorithms for solving large nonsymmetric systems of linear equations. SIAM J. Sci. Comput. 31 (2008/09), no. 2, 1035--1062.

[4] Zemke, Jens-Peter M. Hessenberg eigenvalue-eigenmatrix relations. Linear Algebra Appl. 414 (2006), no. 2-3, 589--606.

[5] Zemke, Jens-Peter M. Abstract perturbed Krylov methods. Linear Algebra Appl. 424 (2007), no. 2-3, 405--434.

[6] Zemke, Jens-Peter M. Variants of IDR with partial orthonormalization. Electron. Trans. Numer. Anal. 46 (2017), 245--272.