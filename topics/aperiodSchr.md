---
bibliography: bib/aperiodSchr.bib
csl : csl/din-1505-2-alphanumeric.csl
---

# Finite Sections of Aperiodic Schrödinger Operators

### Working Groups: aa

### Collaborators (MAT): dgallaun, fgabel, jgrossmann, mlindner, rukena

## Description

Discrete Schrödinger operators are used to describe physical systems on lattices
and, therefore, play an important role in theoretical solid-state physics. 
For a fixed $p \in [1,\infty]$, consider the Schrödinger operator $H \colon \ell^p(\mathbb{Z}) \to \ell^p(\mathbb{Z})$ given by

$$
(H x)_n = x_{n + 1} + x_{n - 1} + v(n) x_nn \in \mathbb{Z},
$$(1)

and its one-sided counterpart $H_+ \colon \ell^p(\mathbb{N}) \to \ell^p(\mathbb{N})$ given by

$$
(H_+ x)_n = x_{n + 1} + x_{n - 1} + v(n) x_n\;,n \in \mathbb{N}, \quad  x_0 = 0\;.
$$ (2)

Based on Definitions $(1)$ and $(2)$, one can associate $H$ and $H_+$ with infinite tridiagonal matrices  $A = (a_{ij})_{i,j \in \mathbb{Z}}$ and $A_+ = (a_{i,j})_{i,j \in \mathbb{N}}$.

Looking at the corresponding infinite linear system of equations

$$
A x = b \quad\text{and}\quad A_+ y = c
$$

it is interesting to know if the solutions $x$ and $y$ to theses systems can be computed approximately by solving the large but finite linear systems

$$
A_m x^{(m)} = b^{(m)} \quad\text{and}\quad (A_+)_m y^{(m)} = c^{(m)}
$$

and letting $m \to \infty$.
This is the main idea of the Finite Section Method (FSM).
In order to assure the applicability of the above procedure, one investigates further properties of the operator $A$, the sequence $(A_n)$ and its one-sided counterparts. In particular, Fredholm Theory, spectral theory and the concept of limit operators play a central role in this investigation [@lindner2006infinite].

This research project deals with the investigation of the applicability of the FSM to problems surging from aperiodic discrete Schrödinger Operators [@gggu2021]. 
A famous example for theses operators is the so called Fibonacci-Hamiltonian [@lindner2018], where the potential $v$ is given as

$$
v(n) := \chi_{[1 - \alpha, 1)}(n \alpha \operatorname{mod} 1)\;, \quad n \in \mathbb{Z}.
$$

For this particular example, the central objects of investigation are periodic approximations $(A_m)$. It is crucial to assure that the spectrum of these approximations eventually avoids the point $0$ for larger numbers of $m$. The following graph shows approximations of the spectra of the one-sided Fibonacci Hamiltonian on $\ell^2(\mathbb{N})$.

![](img/periodicApproxFibHam.png)

## References
