# Queues in an interactive random environment

### Working Groups: aa

### Collaborators (MAT): kkruse, sotten

### Collaborators (External): [Prof. Dr. Hans Daduna](https://www.math.uni-hamburg.de/home/daduna/), [Dr. Ruslan Krenzler](https://www.researchgate.net/profile/Ruslan-Krenzler)

## Description

We consider exponential single server queues with state dependent arrival and service rates which evolve under in uences of external environments. The transitions of the queues are in uenced by the environment's state and the movements of the environment depend on the status of the queues (bi-directional interaction). The environment is constructed in a way to encompass various models from the recent Operations Research literature, where a queue is coupled with an inventory or with reliability issues. With a Markovian joint queueing-environment process we prove separability for a large class of such interactive systems, i.e. the steady state distribution is of product form and explicitly given. The queue and the environment processes decouple asymptotically and in steady state.

For non-separable systems we develop ergodicity criteria via Lyapunov functions. By examples we show principles for bounding throughputs of non-separable systems by throughputs of two related separable systems as upper and lower bound.


![Queues in a random environment](/img/Queues-in-random-environment.png)


## References

[1] Queues in a random environmentlois Theory. S. Otten, R. Krenzler, H. Daduna, K. Kruse. Available at [arXiv:2006.15712](https://arxiv.org/abs/2006.15712
)

[2] Integrated Models for Performance Analysis and Optimization of Queueing-Inventory-Systems in Logistic Networks. S. Otten. Available at [ediss.sub.hamburg](https://ediss.sub.uni-hamburg.de/handle/ediss/7668
)
