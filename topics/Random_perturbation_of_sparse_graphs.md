# Random perturbation of sparse graphs

### Working Groups: dm

### Collaborators (MAT): ymogge

### Collaborators (External): [Max Hahn-Klimroth](https://www.uni-frankfurt.de/70656959/Max-Hahn-Klimroth?), Giulia S. Maesaka, [Samuel Mohr](https://www.fi.muni.cz/~mohr/), [Olaf Parczyk](https://personal.lse.ac.uk/parczyk/)

## Description

In the model of randomly perturbed graphs we consider the union of a deterministic graph $G_\alpha$ with minimum degree $\alpha n$ and the binomial random graph $\mathbb{G}(n,p)$. This model was introduced by Bohman, Frieze, and Martin and for Hamilton cycles their result bridges the gap between Dirac's theorem and the results by Posá and Koršunov on the threshold in $\mathbb{G}(n,p)$. In this note we extend this result in $G_\alpha \cup \mathbb{G}(n,p)$ to sparser graphs with $\alpha=o(1)$. More precisely, for any $\varepsilon>0$ and $\alpha:\mathbb{N} \mapsto (0,1)$ we show that a.a.s. $G_\alpha \cup \mathbb{G}(n,\beta/n)$ is Hamiltonian, where $\beta=−(6+\varepsilon) \log(\alpha)$. If $\alpha>0$ is a fixed constant this gives the aforementioned result by Bohman, Frieze, and Martin and if $\alpha=O(1/n)$ the random part $\mathbb{G}(n,p)$ is sufficient for a Hamilton cycle. We also discuss embeddings of bounded degree trees and other spanning structures in this model, which lead to interesting questions on almost spanning embeddings into $\mathbb{G}(n,p)$.


## References

[1] [M. Hahn-Klimroth, G. Maesaka, Y. Mogge, S. Mohr, and O. Parczyk. Random pertubation of sparse graphs,, arXiv:2004.04672 (2020)](https://arxiv.org/abs/2004.04672)