# Kernel-based reconstruction methods

### Working Groups: aa

### Collaborators (MAT): kalbrecht, mlindner

### Collaborators (External): [Armin Iske](https://www.math.uni-hamburg.de/home/iske/index.html)

## Description

Given a data set $X = \lbrace x_1,...,x_n \rbrace \subset \mathbb{R}^d$  ($d \geq 2$) and values $f_1,...,f_n \in \mathbb{R}$, we want to find a function $s$ such that

$$ f(x_i) = s(x_i) \qquad \forall i \in \lbrace 1,...,n \rbrace.$$

Since there are no *Haar systems* on $\mathbb{R}^d$ ([1],[2]), we cannot simply use polynomials or similar tools from the one-dimensional case. One approach to this problem are **positive definite (kernel) functions** [3, Chapter 8]

$$K:\mathbb{R}^d \times \mathbb{R}^d \to \mathbb{R}.$$ 

For this type of functions, the interpolation problem has a unique solution if we restrict ourselves to

$$s \in \text{span} \lbrace K(\cdot,x_1),..., K(\cdot,x_n) \rbrace.$$

Our research on kernel-based reconstruction methods can be roughly divided into two areas:

* **Well-posedness and convergence of generalized interpolation problems**
Beside standard interpolation problems, positive kernel functions are also useful tools for generalized interpolation problems, i.e. solving the system
$$ \lambda_i(s) = f_{\lambda_i} \qquad \forall i \in \lbrace 1,...,n \rbrace$$ for given functionals $\lambda_i \in \mathcal{H}_K^*$ from the native dual space and values $f_{\lambda_i} \in \mathbb{R}$ [4, Chapter 16]. The main challenge is to guarantee unique solutions for specific choices of functionals and determine, which functions can be approximated arbitrarily well with the reconstruction method. Currently, we are working on *Scattered Radon data interpolation* in the context of image reconstruction, where we build on the method proposed in paper [5].


* **Stabilization methods**
Kernel-based methods usually suffer from bad condition numbers. Reasons for this are often a bad choice of basis functions and/or data sets $X$. We are investigating the construction of stable orthonormal basis [6] and greedy data selection algorithms [7] to increase the stability of kernel-based methods, with applications in generalized interpolation problems.

## References

[1] J. Mairhuber. On Haar’s theorem concerning Chebysheff problems having unique solutions, Proc. Am. Math. Soc. 7 (1956), 609–615

[2] P.C. Curtis. N-parameter families and bestapproximation, Pacific J. Math.9 (1959, 1013–1027

[3] A. Iske. Approximation Theory and Algorithms for Data Analysis. Springer International Publishing, 2018 (Texts in Applied Mathematics)

[4] H. Wendland. Scattered Data Approximation. Cambridge University Press, 2004 (Cam-bridge Monographs on Applied and Computational Mathematics)

[5] S. De Marchi, A. Iske, G. Santin. Image reconstruction from scattered Radon data by weighted positive definite kernel functions. In: Calcolo 55 (2018), 03

[6] S. Müller, R. Schaback. A Newton basis for Kernel spaces. In: Journal of Approximation Theory 161 (2009), S. 645–655

[7] S. De Marchi, R. Schaback, H. Wendland. Near-optimal data-independent point locations for radial basis function interpolation. In: Adv. Comput. Math. 23 (2005), S. 317– 330