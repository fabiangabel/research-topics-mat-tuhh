#!/bin/bash
# test_preprocessor.sh

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_preprocessor
rm -rf $RESEARCH_TESTING/*.log
LOGFILE=$RESEARCH_ROOT/log/preprocessor.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: Preprocessor for Staff 
rm -rf $RESEARCH_TESTING/build
echo "Test 1: Preprocessor for staff keys"
$RESEARCH_ROOT/bin/preproc_staff.sh $RESEARCH_TESTING/aperiodSchr.md $RESEARCH_TESTING/build >> $LOGFILE 2>&1
DIFF=$(diff $RESEARCH_TESTING/build/aperiodSchr.md $RESEARCH_TESTING/aperiodSchr-preproc.md) 
if [ "$DIFF" != "" ] 
then
    echo "Test 1: failed"
    exit 1
else
    echo "Test 1: passed."
fi
#END Test 1

#BEGIN Test 2: Preprocessor for Working Group
rm -rf $RESEARCH_TESTING/build
echo "Test 2: Preprocessor for staff keys"
$RESEARCH_ROOT/bin/preproc_wg.sh $RESEARCH_TESTING/aperiodSchr.md $RESEARCH_TESTING/build >> $LOGFILE 2>&1
DIFF=$(diff $RESEARCH_TESTING/build/aperiodSchr.md $RESEARCH_TESTING/aperiodSchr-preproc-wg.md) 
if [ "$DIFF" != "" ] 
then
    echo "Test 2: failed"
    exit 1
else
    echo "Test 2: passed."
fi
#END Test 2

#BEGIN Test 3: Preprocessor Staff then Working Group
rm -rf $RESEARCH_TESTING/build
echo "Test 3: Preprocessor for staff and working group keys"
$RESEARCH_ROOT/bin/preproc_staff.sh $RESEARCH_TESTING/aperiodSchr.md $RESEARCH_TESTING/build >> $LOGFILE 2>&1
$RESEARCH_ROOT/bin/preproc_wg.sh    $RESEARCH_TESTING/aperiodSchr.md $RESEARCH_TESTING/build >> $LOGFILE 2>&1
DIFF=$(diff $RESEARCH_TESTING/build/aperiodSchr.md $RESEARCH_TESTING/aperiodSchr-preproc-both.md) 
if [ "$DIFF" != "" ] 
then
    echo "Test 3: failed"
    exit 1
else
    echo "Test 3: passed."
fi
#END Test 3

#BEGIN Test 4: Preprocessor Working Group then Staff (opposite order as Test 3)
rm -rf $RESEARCH_TESTING/build
echo "Test 4: Preprocessor for working group and staff keys"
$RESEARCH_ROOT/bin/preproc_wg.sh    $RESEARCH_TESTING/aperiodSchr.md $RESEARCH_TESTING/build >> $LOGFILE 2>&1
$RESEARCH_ROOT/bin/preproc_staff.sh $RESEARCH_TESTING/aperiodSchr.md $RESEARCH_TESTING/build >> $LOGFILE 2>&1
DIFF=$(diff $RESEARCH_TESTING/build/aperiodSchr.md $RESEARCH_TESTING/aperiodSchr-preproc-both.md) 
if [ "$DIFF" != "" ] 
then
    echo "Test 4: failed"
    exit 1
else
    echo "Test 4: passed."
fi
#END Test 4

#this should be the last line (errors exit earlier)
exit 0
