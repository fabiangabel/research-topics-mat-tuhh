#!/bin/bash

# build the files for tuhh-delivery locally
#
#  Structure 
#
# tuhh-build/
# ├── forschung
# │   ├── aa/forschung.html
# │   ├── cm/forschung.html
# │   ├── dm/forschung.html
# │   ├── nm/forschung.html
# │   └── st/forschung.html
# ├── home
# │   ├── ataraz.html
# │   ├── cseifert.html
# │   └── ymogge.html
# └── topics
#     ├── img
#     │   ├── BO_BDO.jpg
#     │   ├── controlimex.png
#     │   └── webpage-example.png
#     ├── topic.html
#     └── Zemke_Hessenberg.html
# 
#
#

# Check commandline arguments
if [ $# -lt 1 ]
then
    RESEARCH_DEPLOY=$RESEARCH_ROOT/build-tuhh
    PROJECT_ROOT="" #standard if webpage is deployed at root
elif [ $# -lt 2 ]
then
    RESEARCH_DEPLOY=$1
    PROJECT_ROOT=""
else
    RESEARCH_DEPLOY=$1
    PROJECT_ROOT=$2
fi
echo "Deploying to $RESEARCH_DEPLOY."
echo "Using PROJECT_ROOT=$PROJECT_ROOT"

RESEARCH_BIN=$RESEARCH_ROOT/bin
RESEARCH_STATIC=$RESEARCH_ROOT/static
RESEARCH_BUILD=$RESEARCH_ROOT/build

rm -rf $RESEARCH_BUILD $RESEARCH_DEPLOY
mkdir -p $RESEARCH_BUILD $RESEARCH_DEPLOY $RESEARCH_DEPLOY/include

echo "Preprocessing Topics..."
$RESEARCH_BIN/preproc_topics.sh "$RESEARCH_BUILD" \
                                "$RESEARCH_ROOT/topics" \
                                "$RESEARCH_BUILD/home" \
                                "$RESEARCH_BUILD/forschung" \
                                "$PROJECT_ROOT" >> $RESEARCH_BUILD/build.log 2>&1

# build index files
#cp $RESEARCH_BUILD/index.md  $RESEARCH_DEPLOY
$RESEARCH_BIN/build_index.sh $RESEARCH_DEPLOY/forschung/topics $RESEARCH_ROOT/topics >> $RESEARCH_BUILD/build.log 2>&1
$RESEARCH_BIN/create_html.sh $RESEARCH_DEPLOY/forschung/topics \
                             $RESEARCH_DEPLOY/forschung/topics \
                             TUHH >> $RESEARCH_BUILD/build.log 2>&1
rm $RESEARCH_DEPLOY/forschung/topics/index.md
#
$RESEARCH_BIN/build_index.sh $RESEARCH_DEPLOY $RESEARCH_ROOT/topics forschung/topics/ >> $RESEARCH_BUILD/build.log 2>&1
$RESEARCH_BIN/create_html.sh $RESEARCH_DEPLOY \
                             $RESEARCH_DEPLOY \
                             TUHH >> $RESEARCH_BUILD/build.log 2>&1
rm $RESEARCH_DEPLOY/index.md

# build html files
echo "Creating HTML Topics with pandoc..."
#topics
$RESEARCH_BIN/create_html.sh $RESEARCH_BUILD/forschung/topics \
                             $RESEARCH_BUILD \
                             TUHH >> $RESEARCH_BUILD/build.log 2>&1
# include ssi directives
echo "Inserting SSI-directives..."
$RESEARCH_BIN/replace_ssi_marker.sh $RESEARCH_DEPLOY/forschung/topics \
                                    $RESEARCH_BUILD/forschung/topics \
                                    $PROJECT_ROOT >> $RESEARCH_BUILD/build.log 2>&1

#delete <h1> tag
$RESEARCH_BIN/postproc_topics.sh $RESEARCH_DEPLOY/forschung/topics

echo "Creating HTML Snippets for Staff..."
#staffpages
$RESEARCH_BIN/create_html.sh $RESEARCH_DEPLOY/home \
                             $RESEARCH_BUILD/home \
                             TUHH >> $RESEARCH_BUILD/build.log 2>&1
$RESEARCH_BIN/postproc_snippets.sh $RESEARCH_DEPLOY/home
# wg pages 
echo "Creating HTML Snippets for WG..."
for wg in aa cm dm nm st
do
    $RESEARCH_BIN/create_html.sh $RESEARCH_DEPLOY/forschung/$wg \
                                 $RESEARCH_BUILD/forschung/$wg \
                                 TUHH >> $RESEARCH_BUILD/build.log 2>&1
    $RESEARCH_BIN/postproc_snippets.sh $RESEARCH_DEPLOY/forschung/$wg
done

# readme
echo "Creating Readme..."
$RESEARCH_BIN/build_README.sh $RESEARCH_STATIC/misc/TUHH_BUILD_README.md \
                              $RESEARCH_DEPLOY/ \
                              >> $RESEARCH_BUILD/build.log 2>&1

echo "Copying files..."
cp -r $RESEARCH_ROOT/topics/img $RESEARCH_DEPLOY/forschung/topics
cp -r $RESEARCH_STATIC/include/{css,javascript,images} $RESEARCH_DEPLOY/include
# replace {include} in html
mkdir -p $RESEARCH_DEPLOY/include/html
for f in $RESEARCH_STATIC/include/html/*.html
do
    filename=$(basename -- "$f")
    sed -e "s;{project_root};$PROJECT_ROOT;" $f > $RESEARCH_DEPLOY/include/html/$filename
done
cp $RESEARCH_STATIC/ssi/htaccess $RESEARCH_DEPLOY/.htaccess
# XBitHack (cf. https://httpd.apache.org/docs/current/mod/mod_include.html)
chmod +x $RESEARCH_DEPLOY/forschung/topics/*.html
chmod +x $RESEARCH_DEPLOY/include/html/*.html

echo "Done."
echo "Start apache by running the command"
echo "  docker run -it --rm --name apache-server -p 8080:80 -v $RESEARCH_DEPLOY:/usr/local/apache2/htdocs/ eltenedor/alpine-httpd-ssi:latest"
echo "Then visit localhost:8080 in your web browser."
echo "Alternatively review file://$RESEARCH_DEPLOY/index.html in your web browser."
