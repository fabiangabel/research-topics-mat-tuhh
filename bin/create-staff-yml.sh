#!/bin/bash
namelist=`grep -h -i -m 1 -r "###\s*Collaborators (MAT):" topics/topic-template.md | sed -e 's/^###\s*Collaborators (MAT):\s*//I' -e 's/\s*,\s*/\n/g'  | sort -u`
for name in $namelist
do
    # pipeline to get full name of staff-member from mat-homepage
    # -> wget the staff homepage of $name
    # -> grep the line with the <h1>-tag, something like <h1>Fabian Gabel, M. Sc.</h1><div class='staffIntro'><p><img src='/home/fgabel/images/portrait.png' title='Foto von Fabian Gabel, M. Sc.' class='staffPicture'></p><div class='staffContact'>
    # -> strip the string such that only the portion between <h1></h1> remains
    # -> remove leading spaces
    
    echo "Found collaborator $name"

    # replace name in .md file
    echo "- pandoc --standalone ./build/$name.md -o ./build/$name.html --mathjax" >> out.yml

done

