#!/bin/bash
# building the a standalone static webpage
RESEARCH_BIN=$RESEARCH_ROOT/bin
RESEARCH_STATIC=$RESEARCH_ROOT/static

# Check commandline arguments
if [ $# -lt 1 ]
then
    RESEARCH_DEPLOY=`pwd`
    RESEARCH_TOPICS=`pwd`"/topics"
    RESEARCH_BUILD=`pwd`"/build"
    echo -e "No deploy directory was specified."
    # clean up directories
    rm -rf $RESEARCH_BUILD
    rm -rf $RESEARCH_DEPLOY/{.public,public}
elif [ $# -lt 2 ] 
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS=$RESEARCH_BUILD
    RESEARCH_BUILD="$1/build"
    echo -e "No extra topics directory was specified."
    # clean up directories
    rm -rf $RESEARCH_BUILD
    rm -rf $RESEARCH_DEPLOY/{.public,public}
elif [ $# -lt 3 ] 
then
    RESEARCH_DEPLOY="$1"
    RESEARCH_TOPICS="$2"
    RESEARCH_BUILD="$1/build"
    echo -e "No extra build directory was specified."
    # clean up directories
    rm -rf $RESEARCH_BUILD
    rm -rf $RESEARCH_DEPLOY/{.public,public}
else
    RESEARCH_DEPLOY="$1"
    RESEARCH_TOPICS="$2"
    RESEARCH_BUILD="$3"
fi

# prepare directories / clean-up
mkdir -p $RESEARCH_DEPLOY
mkdir -p $RESEARCH_BUILD
cp -vf $RESEARCH_TOPICS/*.md $RESEARCH_BUILD

echo -e "Deploying to $RESEARCH_DEPLOY ...;"
echo -e "Building into directory $RESEARCH_BUILD ..."
echo -e "Using .md-files from $RESEARCH_TOPICS ..."

# preprocess .md-files 
echo "Preproc Topics"
$RESEARCH_BIN/preproc_topics.sh $RESEARCH_BUILD $RESEARCH_TOPICS
$RESEARCH_BIN/build_index.sh $RESEARCH_BUILD $RESEARCH_TOPICS

# build html files
echo "Creating HTML Files with pandoc"
$RESEARCH_BIN/create_html.sh $RESEARCH_BUILD 

# replace markers
echo "Replacing markers"
$RESEARCH_BIN/replace_marker.sh $RESEARCH_BUILD 

#make public directory and copy files
mkdir -p $RESEARCH_DEPLOY/.public
for f in $RESEARCH_BUILD/*_final.html
do
    filename=$(basename -- "$f")
    cp $f $RESEARCH_DEPLOY/.public/${filename%_final.html}.html
done

echo "Copying files to final destination ..."
cp -r $RESEARCH_ROOT/{img,css}  $RESEARCH_DEPLOY/.public
mv -v $RESEARCH_DEPLOY/.public  $RESEARCH_DEPLOY/public

echo "Done. Visit file://$RESEARCH_DEPLOY/public/index.html ."
