#!/bin/bash

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_pandoc
rm -rf $RESEARCH_TESTING/test_pandoc.log

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: test pandoc for _research.html
rm -rf $RESEARCH_TESTING/build
echo "Test 1: Pandoc for _research.html"
cd $RESEARCH_TESTING
mkdir -p build
cp *.md build/.
$RESEARCH_ROOT/bin/integrate_pandoc.sh $RESEARCH_TESTING
DIFF=$(diff $RESEARCH_TESTING/build/aperiodSchr_research.html $RESEARCH_TESTING/aperiodSchr_research.html)
if [ "$DIFF" != "" ] 
then
    echo "Test 1: failed"
    exit 1
else
    echo "Test 1: passed."
fi
#END Test 1

#this should be the last line (errors exit earlier)
exit 0

