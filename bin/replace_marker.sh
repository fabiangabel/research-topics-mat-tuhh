#!/bin/bash
# processing command line arguments

# Check commandline arguments
if [ $# -lt 1 ]
then
    RESEARCH_BUILD=`pwd`"/build"
    RESEARCH_TOPICS=`pwd`
    echo -e "No building directory was specified."
elif [ $# -lt 2 ] 
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS=$RESEARCH_BUILD
    echo -e "No extra topics directory was specified."
else
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
fi
mkdir -p $RESEARCH_BUILD
echo -e "Building into directory $RESEARCH_BUILD ..."
echo -e "Including _research.html-files from $RESEARCH_TOPICS ..."
# replace "MARKER" in .html files with research topics
for f in $RESEARCH_ROOT/static/{home,forschung}/*.html
do
    filename=$(basename -- "$f")
    if [ -f "$RESEARCH_TOPICS/${filename%.html}_research.html" ] 
    then
        echo "found $f"
        sed -e "/MARKER/r  $RESEARCH_TOPICS/${filename%.html}_research.html" \
            -e '/MARKER/d' $f > "$RESEARCH_BUILD/${filename%.html}"_final.html
    fi

done

# Building standalone topics and index
RESEARCH_TEMPLATE="$RESEARCH_ROOT/static/topic/topic-template.html"
for f in $RESEARCH_ROOT/topics/*.md $RESEARCH_BUILD/index.md
do
    filename=$(basename -- "$f")
    echo "Looking for $RESEARCH_TOPICS/${filename%.md}_research.html"
    if [ -f "$RESEARCH_TOPICS/${filename%.md}_research.html" ] 
    then
        echo "found $f"
        echo "creating "$RESEARCH_BUILD/${filename%.md}"_final.html"
        sed -e "/MARKER/r $RESEARCH_TOPICS/${filename%.md}_research.html" \
            -e '/MARKER/d' $RESEARCH_TEMPLATE > "$RESEARCH_BUILD/${filename%.md}"_final.html
    fi
done
