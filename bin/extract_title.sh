#!/bin/bash
# extract title of research topic
# extract_title.sh [FILE] [PREFIX]
# [FILE] md-file with title in first line
# [PREFIX] optional prefix for generating html link

filename=$(basename -- "$1")
PREFIX=$2

echo "## `grep '^# ' $1 | sed -e "s;#\s*;\[;g" -e "s;\(.*\);\1\]($PREFIX${filename%.md}.html);g"`"
