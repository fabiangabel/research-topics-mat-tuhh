#!/bin/bash
# build the static webpage locally
BUILD_DIR=$RESEARCH_ROOT/build 
DEPLOY_DIR=$RESEARCH_ROOT
BIN_DIR=$RESEARCH_ROOT/bin

# clean up directories
rm -rf  $BUILD_DIR
rm -rf $DEPLOY_DIR/{.public,public}

#build Index
$BIN_DIR/preproc_topics.sh
$BIN_DIR/buildPandoc.sh

#make public directory and copy files
mkdir -p $DEPLOY_DIR/.public
cp -r $RESEARCH_ROOT/build/*.html $DEPLOY_DIR/.public
cp -r $RESEARCH_ROOT/img $DEPLOY_DIR/.public
cp -r $RESEARCH_ROOT/css $DEPLOY_DIR/.public
mv $DEPLOY_DIR/.public $DEPLOY_DIR/public

echo "Done. Visit file://$DEPLOY_DIR/public/index.html ."

