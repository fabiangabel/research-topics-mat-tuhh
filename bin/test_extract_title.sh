#!/bin/bash
# test_preprocessor.sh

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_preprocessor
rm -rf $RESEARCH_TESTING/*.log
LOGFILE=$RESEARCH_ROOT/log/extract_title.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: Extract title with no prefix
T=1
echo "Test $T: Extract title with no prefix"

#preparation
inputname="input_test_extract_title"
echo "# Title of my topic" > $inputname.md
reftitle="## [Title of my topic]($inputname.html)" 
title=`$RESEARCH_ROOT/bin/extract_title.sh $inputname.md`
if [ "$title" == "$reftitle" ]
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
rm -rf $inputname.*

#BEGIN Test 2: Extract title with no prefix
T=2
echo "Test $T: Extract title with prefix"

#preparation
inputname="input_test_extract_title"
echo "# Title of my topic" > $inputname.md
prefix="../forschung/topics/"
reftitle="## [Title of my topic]($prefix$inputname.html)" 
title=`$RESEARCH_ROOT/bin/extract_title.sh "$inputname".md "$prefix"`
if [ "$title" == "$reftitle" ]
then
    echo "Test $T: PASSED."
else
    echo "Test $T: failed"
    exit 1
fi
rm -rf $inputname.*

#this should be the last line (errors exit earlier)
echo "--------------------------------------------------"
echo "ALL $T tests in $0 PASSED."
exit 0
