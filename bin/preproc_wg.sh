#!/bin/bash
# preproc_wg.sh
# Preprocessor for wg keys in .md topic file
# Replaces key aa by "Lehrstuhl Angewandte Analysis" according to Webpage content
# If Build_dir is passed, titles are appended to corresponding files
# Note: mat.tuhh.de uses WINDOWS-1252 character encoding
# preproc_wg.sh [FILE] [RESEARCH_BUILD] [RESEARCH_STAFF]
#
# #1 [FILE] File to be processed
# #2 [RESEARCH_BUILD] Where the procesing takes place (copy or in-place)
# #3 [RESEARCH_STAFF] Where the wg files are located (titles get appended)

# processing command line arguments
WG_SUFFIX=""
WG_PREFIX=""
if [ $# -lt 2 ]
then
    RESEARCH_BUILD=`pwd`"/build"
    echo -e "No building directory was specified."
elif [ $# -lt 3 ]
then
    RESEARCH_BUILD="$2"
    RESEARCH_STAFF=$RESEARCH_BUILD
    echo -e "No staff directory was specified."
elif [ $# -lt 4 ]
then
    RESEARCH_BUILD="$2"
    RESEARCH_STAFF="$3"
else
    RESEARCH_BUILD="$2"
    RESEARCH_STAFF="$3"
    WG_PREFIX="$4"/
    TOPIC_PREFIX="$4/topics/"
    WG_SUFFIX="/forschung"
fi

mkdir -p $RESEARCH_BUILD
mkdir -p $RESEARCH_STAFF
echo -e "Building into directory $RESEARCH_BUILD ..." # perform (inplace) replacement of keys
echo -e "Building into staff-directory $RESEARCH_STAFF ..." # only prepend titles with links
echo -e "Using root directory $PROJECT_ROOT ..."

filename=$(basename -- "$1")
baseurl='https://www.mat.tuhh.de'
#make output copy
echo "Preprocessing $filename..."
if [ -f "$RESEARCH_BUILD/$filename" ]
then
    echo "File exists, performing preprocessing in place"
else
    cp $1 $RESEARCH_BUILD/$filename
fi

#extract title of topic
echo "Extracting title of $1..."
title=`$RESEARCH_ROOT/bin/extract_title.sh $RESEARCH_BUILD/$filename $TOPIC_PREFIX`
echo "Title of Topic: $title"

# start preprocessing 
# -- leave original untouched, only work with copy in $RESEARCH_BUILD
# replace working group
echo "Replacing working group keys..."
namelist=`grep -h -i -m 1 -r "###\s*Working Groups:" $RESEARCH_BUILD/$filename | sed -e 's/^###\s*Working Groups:\s*//I' -e 's/\s*,\s*/\n/g'  | sort -u`
echo "Found the following keys: " $namelist
for wg in $namelist
do
    #extract full name from tuhh-webpage (deprecated)
    # new version uses files in static
    #fullname=`wget -qO- $baseurl/forschung/$wg | grep h1 | sed -e "s;<h1>\s*\(.*\)</h1>.*$;\1;g" | sed -e 's;^[ \t]*;;'`
    fullname=`cat $RESEARCH_ROOT/static/forschung/$wg.html | grep h1 | sed -e "s;<h1>\s*\(.*\)</h1>.*$;\1;g" | sed -e 's;^[ \t]*;;'`

    echo "Found working group $fullname"

    outname=$wg$WG_SUFFIX
    if [ "$outname" != "$wg" ]
    then
        mkdir -p $RESEARCH_STAFF/$wg
    fi

    # append research to wg-file
    if [ $# -gt 1 ]
    then
        echo -e "\n$title\n" >> $RESEARCH_STAFF/$outname.md
    fi

    outname=$WG_PREFIX$wg$WG_SUFFIX
    sed -i "s;### Working Groups:\(.*\)$wg\(.*\);### Working Groups:\1\[$fullname\]($outname.html)\2;g" $RESEARCH_BUILD/$filename  
    #sed -i "s;### Working Groups:\(.*\)$wg\(.*\);### Working Groups:\1\[$fullname\]($baseurl/forschung/$wg)\2;g" $RESEARCH_BUILD/$filename  #uncomment this line for linking the mat.tuhh.de webpage
done
