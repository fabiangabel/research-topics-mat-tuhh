#!/bin/bash
# run testsuite

RESEARCH_BIN=$RESEARCH_ROOT/bin

echo "Running tests in $RESEARCH_BIN ..."
echo "--------------------------------------------------"
# register tests here
$RESEARCH_BIN/test_preprocessor.sh
$RESEARCH_BIN/test_extract_title.sh
$RESEARCH_BIN/test_create_html.sh
$RESEARCH_BIN/test_replace_marker.sh
$RESEARCH_BIN/test_replace_ssi_marker.sh
$RESEARCH_BIN/test_postproc_snippets.sh
$RESEARCH_BIN/test_build_standalone.sh

echo "All tests in $0 passed."
