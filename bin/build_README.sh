#!/bin/bash
# build_README.sh
# construct README to include
# build_README.sh [README] [RESEARCH_BUILD]
# [README] input file to be processed
# [RESEARCH_BUILD] delivery folder


# Check commandline arguments
if [ $# -lt 2 ]
then
    RESEARCH_BUILD=`pwd`"/build"
else
    RESEARCH_BUILD="$2"
fi
mkdir -p $RESEARCH_BUILD
rm -rf $RESEARCH_BUILD/README.md

if [ -z "$CI_COMMIT_SHORT_SHA" ]; then
  echo "‘CI_COMMIT_SHORT_SHA’ is not set."
  GIT_SHA=`git rev-parse --short HEAD`
  GIT_REF=`git rev-parse --abbrev-ref HEAD`
else
  echo "‘CI_COMMIT_SHORT_SHA’ variable is set."
  GIT_SHA=$CI_COMMIT_SHORT_SHA
  GIT_REF=$CI_COMMIT_REF_NAME
fi


sed -e "s;{DATE};`date --iso-8601=seconds`;" \
    -e "s;{HOST};`whoami`@`hostname`;" \
    -e "s;{GIT_REF};$GIT_REF;" \
    -e "s;{GIT_SHA};$GIT_SHA;" \
    $1 > $RESEARCH_BUILD/README.md
