#!/bin/bash
# processing command line arguments
# replace_ssi_marker.sh
RESEARCH_STATIC=$RESEARCH_ROOT/static

# Check commandline arguments
if [ $# -lt 1 ]
then
    RESEARCH_BUILD=`pwd`"/build"
    RESEARCH_TOPICS=`pwd`
    echo -e "No building directory was specified."
    PROJECT_ROOT=""
elif [ $# -lt 2 ] 
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS=$RESEARCH_BUILD
    echo -e "No extra topics directory was specified."
    PROJECT_ROOT=""
elif [ $# -lt 2 ] 
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
    PROJECT_ROOT=""
else
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
    PROJECT_ROOT="$3"
fi

mkdir -p $RESEARCH_BUILD
echo -e "Building into directory $RESEARCH_BUILD ..."
echo -e "Including .html-files from $RESEARCH_TOPICS ..."
# replace "MARKER" in .html files with research topics
for f in $RESEARCH_TOPICS/*.html
do
    title=`grep '<h1' $f | sed -e "s;<h1 id=\".*\">\(.*\)</h1>;\1;"`
    filename=$(basename -- "$f")
    echo "found $f"
    sed -e "/TOPIC-CONTENT-MARKER/r  $f" \
        -e "s/TOPIC-FILE-MARKER/$filename/ " \
        -e "s/TOPIC-TITLE-MARKER/$title/ " \
        -e '/TOPIC-CONTENT-MARKER/d' \
        -e "s;{project_root};$PROJECT_ROOT;" $RESEARCH_STATIC/ssi/topic_template.shtml > "$RESEARCH_BUILD/${filename%.html}".html
done
