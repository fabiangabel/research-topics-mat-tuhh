#!/bin/bash
# run_pandoc.sh [FILE] [RESEARCH_BUILD] [BUILD_SUFFIX]
# wrapper for pandoc
MATHJAX_URL='https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js'

FULL_FILENAME=$1
RESEARCH_BUILD=$2
BUILD_SUFFIX=$3

filename=$(basename -- "$FULL_FILENAME")

echo "Creating " $RESEARCH_BUILD/"${filename%.md}"$BUILD_SUFFIX.html " ..."
pandoc $FULL_FILENAME  -o  $RESEARCH_BUILD/"${filename%.md}"$BUILD_SUFFIX.html \
    --resource-path=$RESEARCH_ROOT/topics \
    --citeproc \
    --mathjax=$MATHJAX_URL \
    -c css/base.css \
    -c css/extra.css \
    -c "css/print.css" \
    -c "css/superfish.css" \
    -c "css/superfish-vertical.css" \
    -c "css/base_mode.css"