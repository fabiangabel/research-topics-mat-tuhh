#!/bin/bash

if [ $# -lt 1 ]
then
    RESEARCH_BUILD=$RESEARCH_ROOT/static/build
    echo -e "No building directory was specified."
else
    RESEARCH_BUILD="$1"/build
fi
mkdir -p $RESEARCH_BUILD
echo -e "Building into directory $RESEARCH_BUILD ..."


cd $RESEARCH_BUILD

# specify URL to avoid use of local library
# see https://docs.mathjax.org/en/latest/web/start.html#ways-of-accessing-mathjax
# and https://www.jsdelivr.com/package/npm/mathjax
MATHJAX_URL='https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js'

# create plain _research.html to be integrated into other files
for f in *.md
do
    pandoc  $f -o  "${f%.md}"_research.html \
    --mathjax=$MATHJAX_URL \
    -c css/base.css \
    -c css/extra.css \
    -c "css/print.css" \
    -c "css/superfish.css" \
    -c "css/superfish-vertical.css" \
    -c "css/base_mode.css"
done

# replace "MARKER" in .html files with research topics
for f in $RESEARCH_ROOT/static/build/plain/*html
do
    filename=$(basename -- "$f")
    sed -e "/MARKER/r $RESEARCH_ROOT/static/build/${filename%.html}_research.html" -e '/MARKER/d' $RESEARCH_ROOT/static/build/plain/$filename > "${filename%.html}"_final.html
done

# Building standalone topics and index
for f in $RESEARCH_ROOT/topics/*.md $RESEARCH_BUILD/index.md
do
    filename=$(basename -- "$f")
    sed -e "/MARKER/r $RESEARCH_ROOT/static/build/${filename%.md}_research.html" -e '/MARKER/d' $RESEARCH_ROOT/static/topic/topic-template.html > "${filename%.md}"_final.html
done

