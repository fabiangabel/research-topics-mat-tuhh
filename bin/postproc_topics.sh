#!/bin/bash
# post_proc_topics.sh [PROJECT_ROOT]
# apply styling to html snippets

# delete h1 tag as it gets already included by ssi-directive in sidebar.html

if [ $# -lt 1 ]
then
    PROJECT_ROOT=`pwd`
else
    PROJECT_ROOT=$1
fi

for f in $PROJECT_ROOT/*.html 
do
    sed -i  "/<h1.*<\/h1>/d" $f
done
