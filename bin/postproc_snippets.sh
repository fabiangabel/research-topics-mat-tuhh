#!/bin/bash
# post_proc_snippets.sh [PROJECT_ROOT]
# apply styling to html snippets

# replace h2 by div

if [ $# -lt 1 ]
then
    PROJECT_ROOT=`pwd`
else
    PROJECT_ROOT=$1
fi

for f in $PROJECT_ROOT/*.html 
do
    sed -i -e "s;<h2\(.*\)>\(.*\)</h2.*>;<div\1>\2</div>;g" $f
done
