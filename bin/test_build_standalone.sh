#!/bin/bash

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_build_standalone

# prepare place for logfiles
mkdir -p $RESEARCH_ROOT/log
LOGFILE=$RESEARCH_ROOT/log/build_standalone.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: build_standalone with deploy, topics, build arguments" 
T=1
#echo "##################################################"
echo "Test 1: build_standalone with deploy, topics, build arguments" 
#echo "##################################################"
# preparation
RESEARCH_DEPLOY=$RESEARCH_TESTING/deploy
RESEARCH_TOPICS=$RESEARCH_TESTING/tmp_topics
RESEARCH_BUILD=$RESEARCH_TESTING/build
#
rm -rf $RESEARCH_TESTING/{build,deploy}
REF_BUILD=$RESEARCH_TESTING/ref_build
cp -r $RESEARCH_TESTING/topics $RESEARCH_TOPICS
# run test
$RESEARCH_ROOT/bin/build_standalone.sh $RESEARCH_DEPLOY $RESEARCH_TOPICS $RESEARCH_BUILD  >> $LOGFILE 2>&1
# check results
#echo "--------------------------------------------------"
for REF_FILE in $REF_BUILD/*.*
do
    filename=$(basename -- "$REF_FILE")
    DIFF=$(diff $REF_FILE $REF_BUILD/$filename)
    if [ "$DIFF" == "" ] && [ -f "$RESEARCH_BUILD/$filename" ] 
    then
        continue
    else
        echo "Test $T: FAILED."
        echo "Aborting further execution of $0 ..."
        exit 1
    fi
done
echo "Test $T: PASSED."
#END Test 1

#BEGIN Test 2: build_standalone with deploy, topics, build arguments" 
T=2
#echo "##################################################"
echo "Test 2: build_standalone with deploy, topics, build arguments" 
#echo "##################################################"
# preparation
RESEARCH_DEPLOY=$RESEARCH_TESTING/deploy
RESEARCH_TOPICS=$RESEARCH_TESTING/tmp_topics
RESEARCH_BUILD=$RESEARCH_TESTING/build
#
rm -rf $RESEARCH_TESTING/{build,deploy,tmp_topics}
REF_DEPLOY=$RESEARCH_TESTING/ref_deploy
cp -r $RESEARCH_TESTING/topics $RESEARCH_TOPICS
# run test
$RESEARCH_ROOT/bin/build_standalone.sh $RESEARCH_DEPLOY $RESEARCH_TOPICS $RESEARCH_BUILD  >> $LOGFILE 2>&1
# check results
#echo "--------------------------------------------------"
for REF_FILE in $REF_DEPLOY/*.*
do
    filename=$(basename -- "$REF_FILE")
    DIFF=$(diff $REF_FILE $RESEARCH_DEPLOY/public/$filename)
    if [ "$DIFF" == "" ] && [ -f "$RESEARCH_DEPLOY/public/$filename" ] 
    then
        continue
    else
        echo "Test $T: FAILED."
        echo "Aborting further execution of $0 ..."
        exit 1
    fi
done
echo "Test $T: PASSED."
#END Test 2

#this should be the last line (errors exit earlier)
echo "--------------------------------------------------"
echo "ALL $T tests in $0 PASSED."
exit 0
