
#!/bin/bash

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_replace_marker

# prepare place for logfiles
mkdir -p $RESEARCH_ROOT/log
LOGFILE=$RESEARCH_ROOT/log/replace_marker.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: test replace_marker for _research.html with no arguments
T=1
#echo "##################################################"
echo "Test 1: replace_marker with no arguments" 
#echo "##################################################"
# preparation
RESEARCH_BUILD=$RESEARCH_TESTING/build
rm -rf $RESEARCH_TESTING/build
mkdir -p  $RESEARCH_BUILD
REF_FILE=$RESEARCH_TESTING/ref_fgabel_final.html
cd $RESEARCH_TESTING
cp fgabel_research.html $RESEARCH_BUILD
# run test
$RESEARCH_ROOT/bin/replace_marker.sh >> $LOGFILE 2>&1 
# check results
DIFF=$(diff $REF_FILE $RESEARCH_BUILD/fgabel_final.html )
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_BUILD/fgabel_final.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 1

#BEGIN Test 2: test replace_marker for _research.html with build argument
T=2
#echo "##################################################"
echo "Test $T: replace_marker with build argument" 
#echo "##################################################"
# preparation build dir
RESEARCH_BUILD=$RESEARCH_TESTING/build
rm -rf $RESEARCH_BUILD
mkdir -p $RESEARCH_BUILD
#set ref file and copy basefiles
REF_FILE=$RESEARCH_TESTING/ref_fgabel_final.html
cp $RESEARCH_TESTING/*_research.html $RESEARCH_BUILD 
# run test
$RESEARCH_ROOT/bin/replace_marker.sh $RESEARCH_BUILD >> $LOGFILE 2>&1
# check results
DIFF=$(diff $REF_FILE $RESEARCH_BUILD/fgabel_final.html)
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_BUILD/fgabel_final.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 2

#BEGIN Test 3: test replace_marker for _research.html with build and topics argument
T=3
#echo "##################################################"
echo "Test $T: replace_marker with build and plain argument" 
#echo "##################################################"
# preparation build dir
RESEARCH_BUILD=$RESEARCH_TESTING/build
RESEARCH_PLAIN=$RESEARCH_BUILD/plain
rm -rf $RESEARCH_BUILD
mkdir -p $RESEARCH_BUILD && mkdir -p $RESEARCH_PLAIN
#set ref file and copy basefiles
REF_FILE=$RESEARCH_TESTING/ref_fgabel_final.html
cp $RESEARCH_TESTING/fgabel_research.html $RESEARCH_PLAIN 
# run test
$RESEARCH_ROOT/bin/replace_marker.sh $RESEARCH_BUILD $RESEARCH_PLAIN >> $LOGFILE 2>&1 

# check if output exists and results coincide with reference file
DIFF=$(diff $REF_FILE $RESEARCH_BUILD/fgabel_final.html)
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_BUILD/fgabel_final.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 3

#this should be the last line (errors exit earlier)
echo "--------------------------------------------------"
echo "ALL $T tests in $0 PASSED."
exit 0
