#!/bin/bash
# test_postproc_snippets.sh

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_postproc_snippets
rm -rf $RESEARCH_TESTING/*.log
LOGFILE=$RESEARCH_ROOT/log/postproc_snippets.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: Postprocess in pwd
T=1
#echo "##################################################"
echo "Test $T: Postprocess in pwd"
#echo "##################################################"

#preparation
REF_FILE=$RESEARCH_TESTING/ref_test.html 
rm -rf $RESEARCH_TESTING/build
mkdir -p $RESEARCH_TESTING/build
cp $RESEARCH_TESTING/test.html $RESEARCH_TESTING/build
cd $RESEARCH_TESTING/build
$RESEARCH_ROOT/bin/postproc_snippets.sh  >> $LOGFILE 2>&1
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/test.html )
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/test.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
cd $RESEARCH_TESTING
#END Test 1

#BEGIN Test 2: with path
T=2
#echo "##################################################"
echo "Test $T: With project root as argument"
#echo "##################################################"

#preparation
REF_FILE=$RESEARCH_TESTING/ref_test.html 
rm -rf $RESEARCH_TESTING/build
mkdir -p $RESEARCH_TESTING/build
cp $RESEARCH_TESTING/test.html $RESEARCH_TESTING/build
$RESEARCH_ROOT/bin/postproc_snippets.sh $RESEARCH_TESTING/build >> $LOGFILE 2>&1
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/test.html )
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/test.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 2

#this should be the last line (errors exit earlier)
echo "--------------------------------------------------"
echo "ALL $T tests in $0 PASSED."
exit 0
