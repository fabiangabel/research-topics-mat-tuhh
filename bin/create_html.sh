#!/bin/bash
# processing command line arguments
# more than a wrapper for pandoc
# create_html.sh [RESEARCH_BUILD] [RESEARCH_TOPICS]
# [RESEARCH_BUILD] Where the htmls are created
# [RESEARCH_TOPICS] where the .mds for translation are located
# Define variables

# Check commandline arguments
if [ $# -lt 1 ]
then
    RESEARCH_BUILD=`pwd`"/build"
    RESEARCH_TOPICS=`pwd`
    echo -e "No building directory was specified."
    BUILD_SUFFIX="_research"
elif [ $# -lt 2 ] 
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS=$RESEARCH_BUILD
    BUILD_SUFFIX="_research"
    echo -e "No extra topics directory was specified."
elif [ $# -lt 3 ] 
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
    BUILD_SUFFIX="_research"
else
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
    BUILD_SUFFIX=""
fi

RESEARCH_BIN=$RESEARCH_ROOT/bin
mkdir -p $RESEARCH_BUILD
echo -e "Building into directory $RESEARCH_BUILD ..."
echo -e "Using .md-files from $RESEARCH_TOPICS ..."

# specify URL to avoid use of local library
# see https://docs.mathjax.org/en/latest/web/start.html#ways-of-accessing-mathjax
# and https://www.jsdelivr.com/package/npm/mathjax

# create plain _research.html to be integrated into other files
for f in $RESEARCH_TOPICS/*.md
do
    echo $f
    $RESEARCH_BIN/run_pandoc.sh $f $RESEARCH_BUILD $BUILD_SUFFIX
done
