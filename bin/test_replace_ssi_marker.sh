#!/bin/bash
# test_replace_ssi_marker.sh

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_replace_ssi_marker
rm -rf $RESEARCH_TESTING/*.log
LOGFILE=$RESEARCH_ROOT/log/replace_ssi_marker.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: Replace Markers in SSI Directives
T=1
echo "Test $T: Replace Markers in SSI Directives"

#preparation
rm -rf $RESEARCH_TESTING/build
mkdir -p $RESEARCH_TESTING/build
REF_FILE=$RESEARCH_TESTING/ref_test.shtml #use different file ending
cp $RESEARCH_TESTING/test.html $RESEARCH_TESTING/build
$RESEARCH_ROOT/bin/replace_ssi_marker.sh $RESEARCH_TESTING/build $RESEARCH_TESTING >> $LOGFILE 2>&1
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/test.html )
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/test.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi

#this should be the last line (errors exit earlier)
echo "--------------------------------------------------"
echo "ALL $T tests in $0 PASSED."
exit 0
