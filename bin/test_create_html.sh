#!/bin/bash

# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_create_html

# prepare place for logfiles
mkdir -p $RESEARCH_ROOT/log
LOGFILE=$RESEARCH_ROOT/log/create_html.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: test create_html for _research.html with no arguments
T=1
#echo "##################################################"
echo "Test 1: create_html with no arguments" 
#echo "##################################################"
# preparation
REF_FILE=$RESEARCH_TESTING/ref_fgabel_research.html
rm -rf $RESEARCH_TESTING/build
cd $RESEARCH_TESTING
# run test
$RESEARCH_ROOT/bin/create_html.sh >> $LOGFILE 2>&1
# check results
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/fgabel_research.html )
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/fgabel_research.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 1

#BEGIN Test 2: test create_html for _research.html with build argument
T=2
#echo "##################################################"
echo "Test $T: create_html with build argument" 
#echo "##################################################"
# preparation
RESEARCH_BUILD=$RESEARCH_TESTING/build
REF_FILE=$RESEARCH_TESTING/ref_fgabel_research.html
rm -rf $RESEARCH_BUILD
mkdir -p $RESEARCH_BUILD
cp $RESEARCH_TESTING/*.md $RESEARCH_BUILD 
# run test
$RESEARCH_ROOT/bin/create_html.sh  $RESEARCH_BUILD >> $LOGFILE 2>&1
# check results
DIFF=$(diff $REF_FILE $RESEARCH_BUILD/fgabel_research.html)
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_BUILD/fgabel_research.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 2

#BEGIN Test 3: test create_html for _research.html with build and topics argument
T=3
#echo "##################################################"
echo "Test $T: create_html with build and topics argument" 
#echo "##################################################"
# preparation
RESEARCH_BUILD=$RESEARCH_TESTING/build
RESEARCH_TOPICS=$RESEARCH_BUILD/topics
REF_FILE=$RESEARCH_TESTING/ref_fgabel_research.html
rm -rf $RESEARCH_BUILD
mkdir -p $RESEARCH_BUILD && mkdir -p $RESEARCH_TOPICS
cp $RESEARCH_TESTING/*.md $RESEARCH_TOPICS 
# run test
$RESEARCH_ROOT/bin/create_html.sh  $RESEARCH_BUILD $RESEARCH_TOPICS >> $LOGFILE 2>&1
# check if output exists and results coincide with reference file
DIFF=$(diff $REF_FILE $RESEARCH_BUILD/fgabel_research.html)
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_BUILD/fgabel_research.html" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 3

#this should be the last line (errors exit earlier)
echo "--------------------------------------------------"
echo "ALL $T tests in $0 PASSED."
exit 0
