#!/bin/bash
# set environment variable for testing dir
RESEARCH_TESTING=$RESEARCH_ROOT/testing/test_build_index

# prepare place for logfiles
mkdir -p $RESEARCH_ROOT/log
LOGFILE=$RESEARCH_ROOT/log/build_index.log
rm -rf $LOGFILE && touch $LOGFILE

echo "--------------------------------------------------"
echo "Running tests in $0"
echo "--------------------------------------------------"
#BEGIN Test 1: build_index with no arguments
T=1
#echo "##################################################"
echo "Test 1: build_index with no arguments" 
#echo "##################################################"
# preparation
REF_FILE=$RESEARCH_TESTING/ref_index_0_args.md.ref
rm -rf $RESEARCH_TESTING/build
cd $RESEARCH_TESTING
# run test
$RESEARCH_ROOT/bin/build_index.sh >> $LOGFILE 2>&1
# check results
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/index.md )
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/index.md" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 1

#BEGIN Test 2: build_index with build argument
T=2
#echo "##################################################"
echo "Test 2: build_index with build argument" 
#echo "##################################################"
# preparation
REF_FILE=$RESEARCH_TESTING/ref_index_0_args.md.ref
RESEARCH_BUILD=$RESEARCH_TESTING/build
rm -rf $RESEARCH_BUILD
mkdir -p $RESEARCH_BUILD
cp $RESEARCH_TESTING/*.md $RESEARCH_BUILD
# run test
$RESEARCH_ROOT/bin/build_index.sh $RESEARCH_BUILD >> $LOGFILE 2>&1
# check results
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/index.md )
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/index.md" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 2

#BEGIN Test 3: build_index with build and topic argument
T=3
#echo "##################################################"
echo "Test 3: build_index with build and topic argument" 
#echo "##################################################"
# preparation
REF_FILE=$RESEARCH_TESTING/ref_index_2_args.md.ref
RESEARCH_BUILD=$RESEARCH_TESTING/build
RESEARCH_TOPICS=$RESEARCH_TESTING/topics
rm -rf $RESEARCH_BUILD $RESEARCH_TOPICS
mkdir -p $RESEARCH_TOPICS
cp aperiodSchr.md $RESEARCH_TOPICS/
# run test
$RESEARCH_ROOT/bin/build_index.sh $RESEARCH_BUILD $RESEARCH_TOPICS >> $LOGFILE 2>&1
# check results
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/index.md )
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/index.md" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 3

#BEGIN Test 4: build_index with build and topic argument
T=4
#echo "##################################################"
echo "Test 4: build_index with build and topic argument" 
#echo "##################################################"
# preparation
REF_FILE=$RESEARCH_TESTING/ref_index_3_args.md.ref
RESEARCH_BUILD=$RESEARCH_TESTING/build
RESEARCH_TOPICS=$RESEARCH_TESTING/topics
TOPIC_PREFIX=my/prefix/
rm -rf $RESEARCH_BUILD $RESEARCH_TOPICS
mkdir -p $RESEARCH_TOPICS
cp aperiodSchr.md $RESEARCH_TOPICS/
# run test
$RESEARCH_ROOT/bin/build_index.sh $RESEARCH_BUILD $RESEARCH_TOPICS $TOPIC_PREFIX >> $LOGFILE 2>&1
# check results
DIFF=$(diff $REF_FILE $RESEARCH_TESTING/build/index.md )
#echo "--------------------------------------------------"
if [ "$DIFF" == "" ] && [ -f "$RESEARCH_TESTING/build/index.md" ] 
then
    echo "Test $T: PASSED."
else
    echo "Test $T: FAILED."
    echo "Aborting further execution of $0 ..."
    exit 1
fi
#END Test 4

#this should be the last line (errors exit earlier)
echo "--------------------------------------------------"
echo "ALL $T tests in $0 PASSED."
exit 0

