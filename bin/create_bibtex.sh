#!/bin/bash
# create_bibtex.sh [ORCID] [WORKID] [BIBFILE]
# curl wrapper and postprocessing 

ORCID=$1
PUTCODE=$2
BIBFILE=$3

#echo "Querying GET https://pub.orcid.org/v3.0/$ORCID/work/$PUTCODE"
raw_string=`curl --location \
             --request GET "https://pub.orcid.org/v3.0/$ORCID/work/$PUTCODE" \
             --header 'Accept: application/vnd.orcid+json' | grep 'citation-value'`

echo $raw_string \
   | sed -e 's;^.*"citation-value" : ";;' \
         -e 's;"$;;g' \
         -e 's;\\n\\t;\\\n  ;g' \
         -e 's;\\n};};g' \
         -e 's;,\\;,;g' 