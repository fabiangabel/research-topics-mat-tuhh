#!/bin/bash
# processing command line arguments
# Check commandline arguments
# Description: Preprocessor for keys in topics files
# Replace keys for staffmembers and working groups
# 
# preproc_topics.sh [RESEARCH_BUILD] [RESEARCH_TOPICS] [RESEARCH_STAFF] [RESEARCH_WG] [RESEARCH_TUHH]
#
# #1 [RESEARCH_BUILD] where the files are built
# #2 [RESEARCH_TOPICS] where the plain topic files are
# #3 [RESEARCH_STAFF] staffpages
# #4 [RESEARCH_WG] working groups pages
# #5 [RESEARCH_TUHH] optional argument toggling tuhh_build with structure as on webpage
#
RESEARCH_TUHH="" #default value
if [ $# -lt 1 ]
then
    RESEARCH_BUILD=`pwd`"/build"
    RESEARCH_STAFF=$RESEARCH_BUILD  #directory for staff and wg files
    RESEARCH_TOPICS=`pwd`
    RESEARCH_STAFF=$RESEARCH_BUILD
    RESEARCH_WG=$RESEARCH_STAFF
    echo -e "No building directory was specified."
elif [ $# -lt 2 ] 
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS=$RESEARCH_BUILD
    RESEARCH_STAFF=$RESEARCH_BUILD
    RESEARCH_WG=$RESEARCH_STAFF
    echo -e "No extra topics directory was specified."
elif [ $# -lt 3 ]
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
    RESEARCH_STAFF=$RESEARCH_BUILD
    RESEARCH_WG=$RESEARCH_STAFF
elif [ $# -lt 4 ]
then
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
    RESEARCH_STAFF="$3"
    RESEARCH_WG=$RESEARCH_STAFF
else
    RESEARCH_BUILD="$1"
    RESEARCH_TOPICS="$2"
    RESEARCH_STAFF="$3"
    RESEARCH_WG="$4"
    PROJECT_ROOT="$5"
fi

mkdir -p $RESEARCH_BUILD
mkdir -p $RESEARCH_STAFF
mkdir -p $RESEARCH_WG
echo -e "Building into directory $RESEARCH_BUILD ..."
echo -e "Using .md-files from $RESEARCH_TOPICS ..."
echo -e "Building staff-files into directory $RESEARCH_STAFF ..."
echo -e "Building wg-files into directory $RESEARCH_WG ..."
#rm -rf $RESEARCH_BUILD 
baseurl='https://www.mat.tuhh.de'

#build index.html
#replace working group
#echo "Preprocessing Working Groups"
#cp $RESEARCH_ROOT/static/index.md $RESEARCH_BUILD/index.md
#$RESEARCH_ROOT/bin/preproc_wg.sh $RESEARCH_BUILD/index.md $RESEARCH_BUILD >> $RESEARCH_BUILD/build.log 2>&1
#
##replace staff names
#echo "Preprocessing Staff"
#$RESEARCH_ROOT/bin/preproc_staff.sh $RESEARCH_BUILD/index.md $RESEARCH_BUILD >> $RESEARCH_BUILD/build.log 2>&1
#
##finalize file
#sed -i -e "s;\](/img/;\](./img/;g" -e '/PROCESSED AUTOMATICALLY/ d' $RESEARCH_BUILD/index.md

#build rest of the topics
for f in $RESEARCH_TOPICS/*.md
do
    filename=$(basename -- "$f")
    #make output copy
    cp -f $f $RESEARCH_BUILD/$filename

    #replace working group
    $RESEARCH_ROOT/bin/preproc_wg.sh \
        $RESEARCH_BUILD/$filename \
        $RESEARCH_BUILD \
        $RESEARCH_WG  \
        $PROJECT_ROOT/forschung #>> $RESEARCH_BUILD/build.log 2>&1

    #replace staff names
    $RESEARCH_ROOT/bin/preproc_staff.sh \
        $RESEARCH_BUILD/$filename \
        $RESEARCH_BUILD \
        $RESEARCH_STAFF  \
        $PROJECT_ROOT/home  \
        $PROJECT_ROOT/forschung/topics #>> $RESEARCH_BUILD/build.log 2>&1

    #finalize file
    sed -i "s;\](/img/;\](./img/;g" $RESEARCH_BUILD/$filename
done

echo "Finished preprocessing topics."
