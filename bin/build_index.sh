#! /bin/bash
# build_index.sh [RESEARCH_BUILD] [RESEARCH_TOPICS] [TOPIC_PREFIX]
# RESEARCH_BUILD where the index should be built
# RESEARCH_TOPICS where the topics come from
# TOPIC_PREFIX for the hyperlinks

if [ $# -lt 1 ]
then
    RESEARCH_BUILD=`pwd`"/build"
    RESEARCH_TOPICS=`pwd`
    TOPIC_PREFIX=""
    echo -e "No building directory was specified."
elif [ $# -lt 2 ] 
then
    RESEARCH_BUILD=$1
    RESEARCH_TOPICS=$RESEARCH_BUILD
    TOPIC_PREFIX=""
    echo -e "No building topics dir was specified."
elif [ $# -lt 3 ] 
then
    RESEARCH_BUILD=$1
    RESEARCH_TOPICS=$2
    TOPIC_PREFIX=""
    echo -e "No building prefix was specified."
else
    RESEARCH_BUILD=$1
    RESEARCH_TOPICS=$2
    TOPIC_PREFIX=$3
fi

echo -e "Building index into directory $RESEARCH_BUILD ..."
echo -e "Using .md-files from $RESEARCH_TOPICS ..."
echo -e "Prefixing links with $TOPIC_PREFIX ..."

mkdir -p $RESEARCH_BUILD

for f in $RESEARCH_TOPICS/*.md
do
    filename=$(basename -- "$f")
    echo $filename

    #extract title of topic
    title=`$RESEARCH_ROOT/bin/extract_title.sh $RESEARCH_TOPICS/$filename $TOPIC_PREFIX`
    echo "Found topic: $title"

    # append research topic to index
    if [ ! -f $RESEARCH_BUILD/index.md ]
    then
        cp $RESEARCH_ROOT/static/index.md $RESEARCH_BUILD/index.md
    fi
    echo -e "\n$title\n" >> $RESEARCH_BUILD/index.md
done
