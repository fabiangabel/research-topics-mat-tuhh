# Contribute to this Repository

We're glad you read this and want to participate in our repository. If there is anything you want to change about anything in this repository, feel free to reach out.

# Guidelines

* Use GitLab issue/merge-requests at:  [https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/issues/new?issue](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/issues/new?issue) 
* Try to follow [this](https://writemd.rz.tuhh.de/s/bZESwieQR) description.
* Research topics are provided as in [Markdown](https://guides.github.com/features/mastering-markdown/) (file ending `.md`). Check out the corresponding entry in the [repository wiki](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/wikis/Editing-Markdown) for further help in editing markdown.
* Use the [styleguide](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/wikis/Styleguide) in order take a look at template examples an to maintain coherent look of our research topics.

# Help/Contact

* Join the discussion in our Mattermost [Helpdesk](https://communicating.tuhh.de/mathematics/channels/research-topics-helpdesk) (E-10 members only)
* Message [@cfg0846](https://communicating.tuhh.de/entrance/messages/@cfg0846) on [TUHH-Mattermost](https://communicating.tuhh.de/)
* Email [Fabian Gabel](mailto:fabian.gabel@tuhh.de) [`pgp-pubkey`](https://www.mat.tuhh.de/home/fgabel/D4D6E4396A99138212A8E453FD8077FAB05DA33F.asc)
