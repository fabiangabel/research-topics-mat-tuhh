
## [Research Interests](index.html)


## [Observability and controllability for systems in Banach spaces](Control_in_Banach_spaces.html)


## [Finite Sections of Aperiodic Schrödinger Operators](aperiodSchr.html)


## [Stokes Operator on Lipschitz Domains](navier-stokes.html)


## [Research Project Template](topic-template.html)

