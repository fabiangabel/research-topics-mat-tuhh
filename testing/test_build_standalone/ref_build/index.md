# Research Interests

This page hopefully features the past, current and future research interests of our institute.

You browse this webpage either by clicking on a **research topic**, a **working group** or a particular **researcher**.

If you are looking for a guided tour, watch [https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4](https://media.tuhh.de/e10/gabel/research-topics/video1_rundgang.mp4)

The master webpage is currently delivered to [https://fabiangabel.gitlab.io/research-topics-mat-tuhh](https://fabiangabel.gitlab.io/research-topics-mat-tuhh).

The deveolpment version of this webpage for review is currenctly delivered to [https://review.fabian-gabel.de/dev](https://review.fabian-gabel.de/dev).

The corresponding GitLab repository is hosted at [https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh) and push-mirrored to [https://gitlab.com/fabiangabel/research-topics-mat-tuhh](https://gitlab.com/fabiangabel/research-topics-mat-tuhh)

If you want to contribute a research topic, see the [CONTRIBUTING.md](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/blob/master/CONTRIBUTING.md).


### Working Groups: [Lehrstuhl Angewandte Analysis](aa.html), [Lehrstuhl Computational Mathematics](cm.html), [Lehrstuhl Diskrete Mathematik](dm.html), [Lehrstuhl Numerische Mathematik](nm.html), [Lehrstuhl Stochastik](st.html)

### Collaborators (MAT): [Prof. Dr. Anusch Taraz](ataraz.html), [Dr. habil. Christian Seifert](cseifert.html), [Dr. Dennis Clemens](dclemens.html), [Dennis Gallaun, M. Sc.](dgallaun.html), [Prof. Dr. Daniel Ruprecht](druprecht.html), [Dipl.-Ing. Frank Bösch](fboesch.html), [Dr. Florian Bünger](fbuenger.html), [Fabian Gabel, M. Sc.](fgabel.html), [Fabian Hamann, M. Sc.](fhamann.html), [Dr. habil. Haibo Ruan](hruan.html), [Prof. Dr. Heinrich Voß](hvoss.html), [Judith Angel, M. Sc.](jangel.html), [Jorin Dornemann, M. Sc.](jdornemann.html), [Joscha Fregin, M. Sc.](jfregin.html), [Jonas Grams, M. Sc.](jgrams.html), [Dr. Julian Großmann](jgrossmann.html), [Jan Meichsner](jmeichsner.html), [Dr. Jens-Peter M. Zemke](jpmzemke.html), [Julio Urizarna, M. Sc.](jurizarna.html), [Kristof Albrecht](kalbrecht.html), [Katharina Klioba, M. Sc.](kklioba.html), [Dr. Karsten Kruse](kkruse.html), [Margitta Janssen](mjanssen.html), [Prof. Dr. Marko Lindner](mlindner.html), [Prof. Dr. Matthias Schulte](mschulte.html), [Marco Wolkner](mwolkner.html), [Peter Baasch, M. Sc.](pbaasch.html), [Pranshu Gupta, M. Sc.](pgupta.html), [Rebekka Beddig, M. Sc.](rbeddig.html), [Riko Ukena, M. Sc.](rukena.html), [Dr. Sebastian Götschel](sgoetschel.html), [Prof. Dr. Sabine Le Borne](sleborne.html), [Dr. Sonja Otten](sotten.html), [Thore Saathoff, M. Eng.](tsaathoff.html), [Vincent Griem, M. Sc.](vgriem.html), [Vanessa Trapp, M. Sc.](vtrapp.html), [Willi Leinen, M. Sc.](wleinen.html), [Prof. Dr. Wolfgang Mackens](wmackens.html), [Yannick Mogge, M. Sc.](ymogge.html)

## Research Topics

## [Observability and controllability for systems in Banach spaces](Control_in_Banach_spaces.html)

