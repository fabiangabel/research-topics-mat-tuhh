# Download base image ubuntu 20.04
FROM ubuntu:20.04

# LABEL about the custom image
LABEL maintainer="fabian.gabel@tuhh.de"
LABEL version="0.1"
LABEL description="This is custom Docker Image for \
Website deployment with bash preprocessor and pandoc."

# Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

# Update Ubuntu Software repository
RUN apt update

# Install nginx, php-fpm and supervisord from ubuntu repository
RUN apt -y install pandoc 

CMD /bin/bash