# Download base image alpine 
FROM alpine:latest

# LABEL about the custom image
LABEL maintainer="fabian.gabel@tuhh.de"
LABEL version="0.1"
LABEL description="This is custom Docker Image for \
alpine with further tools in order to use openssh/rsync and bash."

# Disable Prompt During Packages Installation

# Update Ubuntu Software repository
RUN apk update 
RUN apk add openssh-client 
RUN apk add bash
RUN apk add rsync

CMD /bin/bash