FROM pandoc/core:latest

RUN apk update
RUN apk add bash

ENTRYPOINT ["/bin/bash"]
