This build was delivered on `{DATE}` by `{HOST}`.

It resembles the information from [https://collaborating.tuhh.de/](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh) at

| GIT REF NAME | GIT SHORT SHA |
| -- | -- |
| `{GIT_REF}` | `{GIT_SHA}` |

The folder structure is as follows:
```
build-tuhh
├── forschung
│   ├── aa
│   ├── cm
│   ├── dm
│   ├── nm
│   ├── st
│   └── topics
│       └── img
├── home
└── include
    ├── css
    ├── html
    ├── images
    └── javascript
```

The files in `/home` and `/forschung/{aa,cm,dm,nm,st}` contain `.html` snippets to be included in the respective pages on the webserver.

The files in `/forschung/topics` are standalone research topic-`.html` files with SSI directives.

The files in `/forschung/topics/img` are assets from `/forschung/topics`.

The files in `/include` are only for testing purpose in order to make the delivery self-contained. 
They should already reside in the respective directory at the production site.

In case of problems, refer to the [project wiki](https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh/-/wikis/home).

For verification purposes use the Docker container [`eltenedor/alpine-httpd-ssi`](https://hub.docker.com/repository/docker/eltenedor/alpine-httpd-ssi) from within `build-tuhh`

```bash
docker run -it --rm --name apache-server -p 8080:80 -v `pwd`:/usr/local/apache2/htdocs/ eltenedor/alpine-httpd-ssi
```

Then visit [localhost:8080](http://localhost:8080/) with your webbrowser.
