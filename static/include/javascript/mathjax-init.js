  MathJax.Hub.Config({
    extensions: ["tex2jax.js"],
    jax: ["input/TeX", "output/HTML-CSS"],
		 /* set zoom trigger  and scale*/
    menuSettings: {
      zoom: "Click",
			zscale: "250%"
    },
		showProcessingMessages:"false",
		messageStyle: "none",
    tex2jax: {
      inlineMath: [ ['$','$'], ["\\(","\\)"] ],
      displayMath: [ ['$$','$$'], ["\\[","\\]"] ],
      processEscapes: true
    }   
  });
