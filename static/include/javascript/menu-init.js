(function($){ //create closure so we can safely use $ as alias for jQuery
	$(document).ready(function(){
		// initialise plugin
		var example = $('#nav').superfish({
			//add options here if required
			cssArrows: false
		});
	});

})(jQuery);
