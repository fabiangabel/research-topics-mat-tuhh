function toggleMe(a) {
	var e = document.getElementById(a);
	// Prüfen ob das Element existiert
	if (!e)
		return true;

	// Umschalten
	if(e.style.display=="none") {
		e.style.display="block"
	} else {
		e.style.display="none"
	}
	return true;
}

// Objekte per Click ein- und ausblenden.
// Auslöser müssen die Klasse "toggleMe" haben.
// Die ID des Ziels ist in data-target zu definieren.
$(function() {
	$(".toggleMe").click(function() {
		console.log("click");
		$("#" + $(this).data("target")).slideToggle("slow");
	});
});
