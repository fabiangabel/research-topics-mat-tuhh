<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8"/>
  <title>Review App</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/picnic">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div style="padding: 15px 25px 75px">

<h1>Review apps for <a href="https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh">research-topics-mat-tuhh</a></h1>

<h2>The following is a list of all branches with a review app:</h2>

<?php
function getSubDirectories($dir)
{
  $subDir = array();
  $directories = array_filter(glob($dir), 'is_dir');
  $subDir = array_merge($subDir, $directories);
  foreach ($directories as $directory) $subDir = array_merge($subDir, getSubDirectories($directory.'/*'));
  return $subDir;
}


$d = dir(".");
echo "<ul>";

while (false !== ($entry = $d->read()))
{
  if (is_dir($entry) && ($entry != '.') && ($entry != '..')) {
      echo "<li>$entry" . " (last changed: " . date("F d Y H:i:s", filectime($entry)). ")</li>";
      #$directories = glob('/home/gabel/review.fabian-gabel.de' . '/*' , GLOB_ONLYDIR);
      $directories = glob($entry . "/*", GLOB_ONLYDIR);
      usort($directories, function($a, $b) {
          return filemtime($b) - filemtime($a);
      });
      echo "<ul>";
     for ($i = 0; $i < sizeof($directories) ; $i++){
      $hash=basename($directories[$i]);
      echo "<li><a href='{$directories[$i]}'>{$hash}</a>" . " (last changed: " . date("F d Y H:i:s", filectime($directories[$i])). ")</li>";
     }
     echo "</ul>";
  }
}
echo "</ul>";
$d->close();
?>

<!-- Footer -->
<hr>
<footer class="page-footer font-small blue">

  <!-- Copyright -->
  <a href="https://collaborating.tuhh.de/cfg0846/research-topics-mat-tuhh">GitLab Repository</a>
  |
  <a href="/impressum.html">Impressum</a>
  |
  <a href="https://uberspace.de/en/">Hosted on Asteroids</a>
  </div>
  <!-- Copyright -->

  </footer>
  <!-- Footer -->
</div>
</html>
